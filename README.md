# spring-boot-study

#### 项目介绍
sprig boot学习教程配套源代码

#### 软件架构

* Intellij IDEA 2017.2
* jdk 1.8
* Maven 3.3.9
* Tomcat 8.5
* Mysql 5.7
* Navicat for Mysql
* Google浏览器


#### 技术选型

* 核心框架：Spring Boot 1.5.3
* 视图框架：Spring MVC 4.3.5
* 持久层框架：Mybatis 3.4.2 + 通用Mapper 3.3.9
* 服务端验证：Hibernate validator 5.2.4
* API生成工具: Swagger2
* 单元测试：Junit 4.12
* 模板引擎：Thymeleaf 3.0.0
* 数据库连接池：Alibaba Druid 1.0.18
* 缓存框架：Spring Cache + Ehcache 2.5.3
* 日志管理：SLF4J 1.7.22 + Logback 1.1.8
* 分页插件：PageHelper 5.0.0
* 工具类：Apache Commons、Jackson 2.2、Lombok 1.16.14、Hutool 2.16.0

#### 使用说明

1. 本源码配套教程在简书上地址:
[星缘1314得简书](https://www.jianshu.com/u/13a6267912e6)
2. 直接通过git clone代码到本地运行每个模块的核心启动类即可测试每项功能

#### 大家可以关注我的写作平台

个人网站：[https://www.zhangbox.cn](https://www.zhangbox.cn)

CSDN ：[https://blog.csdn.net/qq_16415417](https://blog.csdn.net/qq_16415417)

简书 ：[https://www.jianshu.com/u/13a6267912e6](https://www.jianshu.com/u/13a6267912e6)

segmentfault ：[https://segmentfault.com/u/xingyuan_59b0b68ebe448/articles](https://segmentfault.com/u/xingyuan_59b0b68ebe448/articles)

欢迎关注我的**微信公众号**获取更多更全的学习资源，视频资料，技术干货！
![欢迎扫码关注](https://img-blog.csdnimg.cn/20190520141934670.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzE2NDE1NDE3,size_16,color_FFFFFF,t_70)![资源领取方式](https://img-blog.csdnimg.cn/20190520142034116.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzE2NDE1NDE3,size_16,color_FFFFFF,t_70)
公众号回复“**学习**”，拉你进**程序员技术讨论群**，**干货资源**第一时间分享。

公众号回复“**视频**”，领取**800GJava**视频学习资源。

公众号回复“**全栈**”，领取**1T前端**，**Java**，**产品经理**，**微信小程序**，**Python**等资源合集大放送。

公众号回复“**慕课**”，领取**1T慕课实战**学习资源。

公众号回复“**实战**”，领取**750G项目实战**学习资源。

公众号回复“**面试**”，领取**20G面试实战**学习资源。

 ##### QQ群：
![QQ交流学习群](https://img-blog.csdnimg.cn/2019052014211777.png)
 ##### QQ群中你可以获得的福利有：

> 1.最全的学习资源，最多的学习大佬！
> 2.最全的学习升级帮助，帮助你快速打怪升级。
> 3.最重要是提供就业指导以及兼职机会！
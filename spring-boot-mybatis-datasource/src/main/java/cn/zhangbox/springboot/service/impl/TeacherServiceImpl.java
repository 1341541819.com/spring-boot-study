package cn.zhangbox.springboot.service.impl;

import cn.zhangbox.springboot.dao.teacher.TeacherDao;
import cn.zhangbox.springboot.entity.Teacher;
import cn.zhangbox.springboot.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangyuan
 * @since 2017-07-06
 *
 */
@Service("TeacherService")
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	TeacherDao teacherDao;

	@Override
	public List<Teacher> getTeacherList(String tname, Integer age) {
		return teacherDao.getTeacherList(tname,age);
	}
}

package cn.zhangbox.springboot.service;

import cn.zhangbox.springboot.entity.Student;
import cn.zhangbox.springboot.entity.Teacher;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhangyuan
 * @since 2018-07-06
 */
public interface TeacherService {

    /**
     * 查询所有的学生信息
     *
     * @param tname
     * @param age
     * @return
     */
    List<Teacher> getTeacherList(String tname, Integer age);
}

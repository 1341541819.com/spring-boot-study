package cn.zhangbox.springboot.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * teacher实体类
 * </p>
 *
 * @author zhangyuan
 * @since 2018-07-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Teacher {

    private static final long serialVersionUID = 1L;

	/**
     * 主键id
     */
	private Integer tno;
    /**
     * 老师姓名
     */
	private String tname;
    /**
     * 性别
     */
	private String sex;
    /**
     * 生日
     */
	private String birth;
    /**
     * 年龄
     */
	private String age;
    /**
     * 简介
     */
	private String dept;

}

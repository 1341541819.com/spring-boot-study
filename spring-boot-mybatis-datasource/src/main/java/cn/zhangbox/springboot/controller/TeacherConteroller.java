package cn.zhangbox.springboot.controller;

import cn.zhangbox.springboot.entity.Teacher;
import cn.zhangbox.springboot.service.TeacherService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by zhangyuan on 2018/07/06
 */
@Controller
@RequestMapping("/teacher")
public class TeacherConteroller {
    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherConteroller.class);

    @Autowired
    protected TeacherService teacherService;

    /**
     * 查询所有的老师信息
     *
     * @param tname
     * @param age
     * @param modelMap
     * @return
     */
    @ResponseBody
    @GetMapping("/list")
    public String list(String tname, Integer age, ModelMap modelMap) {
        String json = null;
        try {
            List<Teacher> teacherList = teacherService.getTeacherList(tname, age);
            modelMap.put("ren_code", "0");
            modelMap.put("ren_msg", "查询成功");
            modelMap.put("teacherList", teacherList);
            json = JSON.toJSONString(modelMap);
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.put("ren_code", "0");
            modelMap.put("ren_msg", "查询失败===>" + e);
            LOGGER.error("查询失败===>" + e);
            json = JSON.toJSONString(modelMap);
        }
        return json;
    }
}

package cn.zhangbox.springboot.dao.teacher;

import cn.zhangbox.springboot.entity.Student;
import cn.zhangbox.springboot.entity.Teacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhangyuan
 * @since 2017-07-06
 */
public interface TeacherDao {

	List<Teacher> getTeacherList(@Param("tname") String tname, @Param("age") Integer age);

}
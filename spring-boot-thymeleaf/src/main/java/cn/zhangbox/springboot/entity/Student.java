package cn.zhangbox.springboot.entity;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author wangl
 * @since 2017-10-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class Student {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private @ApiParam(value = "学生编号")
    Integer sno;
    /**
     * 学生姓名
     */
    private @ApiParam(value = "学生姓名")
    String sname;
    /**
     * 性别
     */
    private @ApiParam(value = "性别")
    String sex;
    /**
     * 生日
     */
    private @ApiParam(value = "生日")
    String birth;
    /**
     * 年龄
     */
    private @ApiParam(value = "年龄")
    String age;
    /**
     * 简介
     */
    private @ApiParam(value = "简介")
    String dept;
    /**
     * 创建时间
     */
    private @ApiParam(value = "创建时间")
    String createTime;
    /**
     * 更新时间
     */
    private @ApiParam(value = "更新时间")
    String updateTime;

}

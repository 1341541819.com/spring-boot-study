package cn.zhangbox.springboot.service.impl;

import cn.zhangbox.springboot.dao.StudentDao;
import cn.zhangbox.springboot.entity.Student;
import cn.zhangbox.springboot.service.StudentService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @since 2019-04-15
 */
@Service("StudentService")
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentDao studentDao;

    @Override
    public PageInfo<Student> getStudentList(Integer pageNum, Integer pagSize, String sname, Integer age) {
        PageInfo<Student> pageInfo = null;
        PageHelper.startPage(pageNum, pagSize);
        try {
            List<Student> studentList = studentDao.getStudentList(sname, age);
            pageInfo = new PageInfo<Student>(studentList);
        } catch (Exception e) {
            log.error("持久层查询学生信息发生了异常={}", e);
        }
        return pageInfo;
    }

    /**
     * 保存学生信息
     *
     * @param student
     * @return
     */
    @Override
    public Integer save(Student student) {
        Integer integer = null;
        try {
            integer = studentDao.save(student);
        } catch (Exception e) {
            log.error("错误了=====>" + e);
        }
        return integer;
    }

    /**
     * 根据id查询学生信息
     *
     * @param sno
     * @return
     */
    @Override
    public Student getStudentById(int sno) {
        Student student = null;
        try {
            student = studentDao.getStudentById(sno);
        } catch (Exception e) {
            log.error("错误了=====>" + e);
        }
        return student;
    }

    /**
     * 更新学生信息
     *
     * @param student
     * @return
     */
    @Override
    public Integer update(Student student) {
        Integer integer = null;
        try {
            integer = studentDao.update(student);
        } catch (Exception e) {
            log.error("错误了=====>" + e);
            e.printStackTrace();
        }
        return integer;
    }

    /**
     * 删除学生信息
     *
     * @param id
     * @return
     */
    @Override
    public Integer updateStatus(String id) {
        Integer integer = null;
        try {
            integer = studentDao.updateStatus(id);
        } catch (Exception e) {
            log.error("错误了=====>" + e);
            e.printStackTrace();
        }
        return integer;
    }
}

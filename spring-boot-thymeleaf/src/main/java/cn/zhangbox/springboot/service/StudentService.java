package cn.zhangbox.springboot.service;

import cn.zhangbox.springboot.entity.Student;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhangyuan
 * @since 2018-07-06
 */
public interface StudentService {

    /**
     * 查询所有的学生信息
     *
     * @param pageNum
     * @param pagSize
     * @param sname
     * @param age
     * @return
     */
    PageInfo<Student> getStudentList(Integer pageNum, Integer pagSize, String sname, Integer age);

    /**
     * 保存学生信息
     *
     * @param student
     * @return
     */
    Integer save(Student student);

    /**
     * 根据id查询学生信息
     *
     * @param sno
     * @return
     */
    Student getStudentById(int sno);

    /**
     * 更新学生信息
     *
     * @param student
     * @return
     */
    Integer update(Student student);

    /**
     * 删除学生信息
     *
     * @param id
     * @return
     */
    Integer updateStatus(String id);
}

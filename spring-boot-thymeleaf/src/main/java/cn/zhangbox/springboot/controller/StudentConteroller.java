package cn.zhangbox.springboot.controller;

import cn.zhangbox.springboot.entity.Student;
import cn.zhangbox.springboot.service.StudentService;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zhangyuan on 2018/07/06
 */
@Controller
@RequestMapping("/student")
@Api(value = "swagger-service", description = "学生相关接口")
@Slf4j
public class StudentConteroller extends BaseController{
    private static final String BASE_PATH = "student/";
    @Autowired
    protected StudentService studentService;

    /**
     * 查询所有的学生信息
     *
     * @param sname
     * @param age
     * @param modelMap
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "查询所有学生", notes = "查询所有学生")
    public String list(
            @ApiParam(value = "页码数") @RequestParam(defaultValue = "1") Integer pageNum,
            @ApiParam(value = "每页数量") @RequestParam(defaultValue = "10") Integer pagSize,
            @ApiParam(value = "学生名称") @RequestParam(required = false) String sname,
            @ApiParam(value = "年龄") @RequestParam(required = false) Integer age,
            ModelMap modelMap) {
        String json = null;
        try {
            PageInfo<Student> pageInfo = studentService.getStudentList(pageNum, pagSize, sname, age);
            modelMap.put("ren_code", "0");
            modelMap.put("ren_msg", "查询成功");
            modelMap.put("pageInfo", pageInfo);
            modelMap.put("sname", sname);
            modelMap.put("age", age);
            return BASE_PATH + "student-list";
        } catch (Exception e) {
            log.error("跳转失败===>" + e);
            json = JSON.toJSONString(modelMap);
            return "common/500";
        }
    }

    /**
     * 跳转到学生信息添加页面
     *
     * @return
     */
    @GetMapping(value = "/to/add")
    @ApiOperation(value = "跳转到学生信息添加页面", notes = "跳转到学生信息添加页面")
    public String toadd(ModelMap modelMap) {
        try {
            log.info("跳转到学生信息添加页面!");
            return BASE_PATH + "student-add";
        } catch (Exception e) {
            log.error("跳转失败! e = {}", e);
            return "admin/common/404";
        }
    }

    /**
     * 添加学生信息
     *
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/add")
    @ApiOperation(value = "添加学生信息", notes = "添加学生信息")
    public ModelMap add(Student student) {
        ModelMap messagesMap = new ModelMap();
        try {
            log.debug("添加学生信息参数! couponAuthTemplate = {}", student.toString());
            Integer insertStatus = studentService.save(student);//保存数据
            if (insertStatus > 0) {
                log.info("添加学生信息成功! StockCode = {}");
                messagesMap.put("status", SUCCESS);
                messagesMap.put("message", "添加成功!");
                return messagesMap;
            } else {
                log.info("添加学生信息失败, 但没有抛出异常! StockCode = {}");
                messagesMap.put("status", FAILURE);
                messagesMap.put("message", "添加失败!");
                return messagesMap;
            }
        } catch (Exception e) {
            log.error("添加学生信息失败!  e = {}", e);
            messagesMap.put("status", FAILURE);
            messagesMap.put("message", "添加失败!");
            return messagesMap;
        }
    }

    /**
     * 跳转到学生信息编辑页面
     *
     * @return
     */
    @GetMapping(value = "/edit/{id}")
    @ApiOperation(value = "跳转到学生信息编辑页面", notes = "跳转到学生信息编辑页面")
    public String edit(@PathVariable("id") String id, ModelMap modelMap){
        Student student = null;
        if (id != null) {
            student = studentService.getStudentById(Integer.parseInt(id));
        } else {
            log.error("跳转到学生信息编辑页面失败! Id为空");
            return "admin/common/404";
        }
        log.info("跳转到学生信息编辑页面！id = {}", id);
        modelMap.put("student", student);
        return BASE_PATH + "student-edit";
    }

    /**
     * 更新学生信息信息
     *
     * @param id 奖励ID
     * @return
     */
    @ResponseBody
    @PutMapping(value = "/update/{id}")
    @ApiOperation(value = "更新学生信息", notes = "更新学生信息")
    public ModelMap updateUser(@ApiParam(value = "学生id") @PathVariable("id") Integer id, Student student) {
        ModelMap messagesMap = new ModelMap();
        try {
            log.debug("编辑学生信息参数! student = {}", student.toString());
            //封装参数
            if (id != null) {
                student.setSno(id);
            }
            Integer update = studentService.update(student);
            if (update > 0) {
                log.info("编辑学生信息成功! id= {},student = {}", id, student.toString());
                messagesMap.put("status", SUCCESS);
                messagesMap.put("message", "编辑成功!");
                return messagesMap;
            } else {
                log.info("编辑学生信息失败,但没有抛出异常 ! id= {}, couponauthtemplate = {}", id, student.toString());
                messagesMap.put("status", FAILURE);
                messagesMap.put("message", "编辑失败!");
                return messagesMap;
            }
        } catch (Exception e) {
            log.error("编辑学生信息失败! id = {},  couponauthtemplate = {}, e = {}", id, student.toString(), e);
            messagesMap.put("status", FAILURE);
            messagesMap.put("message", "编辑学生信息失败!");
            e.printStackTrace();
            return messagesMap;
        }
    }

    /**
     * 更新删除状态
     *
     * @param
     * @return
     */
    @DeleteMapping("/status/{id}")
    @ResponseBody
    @ApiOperation(value = "删除学生信息", notes = "删除学生信息")
    public ResponseEntity<String> dele(@ApiParam(value = "学生id") @PathVariable("id") String id, String status) {
        try {
            log.debug("删除学生信息参数! shopping = {}");
            Integer updateStatus = studentService.updateStatus(id);
            if (updateStatus > 0) {
                log.info("删除学生信息成功! ProdCode = {}");
                return ResponseEntity.status(HttpStatus.OK).build();
            }
            log.info("删除学生信息失败, 但没有抛出异常! ProdCode = {}");

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();

        } catch (Exception e) {
            log.error("删除学生信息失败! shopping = {}, e = {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
